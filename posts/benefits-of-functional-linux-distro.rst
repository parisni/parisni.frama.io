.. title: Benefits of functional linux distro
.. slug: benefits-of-functional-linux-distro
.. date: 2019-08-17 23:08:26 UTC+02:00
.. tags: nix, nixos, guix, linux
.. category: system administration 
.. link: 
.. description: This is about the first contact with nix
.. type: text


Operating System is a tool to build and create other tools. OS is secondary and
the perfect one should be less present as possible. Less time fighting with
installation, less time with configuration, less time with frustration.

.. END_TEASER

My first contact with linux has been ubuntu, and I am still using it at work:
using a mainstream tool, is a security and the promize to find out solution
fast on forum, blogs. When comes new version of ubuntu LTS, this is a pain to
migrate, and always a lost of energy so it is usual to be frustrated and
keeping and old OS with old software dependency.

Then I gave a try to rolling-release distribution: Archlinux. The good point is
you never late and get the latest version on anything. However I felt that OS
was taking more and more importance in my daily practice ; any package upgrade
was a critical action. I finally become very quiet with upgrade, and
paradoxally late again with software release.

By chance I recently discovered functional programming in general, and
functional OS in particular. Nixos and Guix are both exploiting the idea of
immutability to build a very robust OS design. This allows to rollback any bad
situation very easily. The cost of this power is to learn how to master the
beast: haskell or scheme programming language respectively. An I d'say that's
an opportunity to learn new languages of interest.

While guix is more appealing for its full-GNU support I choosed nixos. The
reason is the packet manager called nix can be installed and used on any
distribution. Therefore I can take my time to learn haskell and keep ubuntu as
a security for daily primary objective: having a transparent and robust OS.


