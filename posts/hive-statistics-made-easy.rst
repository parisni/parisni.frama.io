.. title: Hive statistics made easy
.. slug: hive-statistics-made-easy
.. date: 2019-06-06 20:07:49 UTC+02:00
.. tags: hive, big-data
.. category: data engineering
.. link: 
.. description: 
.. type: text


Apache hive stores the tables informations in its metadata database so called
the Metastore.  This database contains many useful information such tables,
columns, row numbers, types. The hive 3 version also contains informations
about the primary/foreign keys.

.. END_TEASER

Numbers of rows 
===============

Like any query on the metastore, the result is instant. This is based on
analysed tables. 

.. code:: sql

	select "NAME", "TBL_NAME", "PARAM_VALUE" , "TBL_TYPE" 
	from "TABLE_PARAMS" 
	join "TBLS" using ("TBL_ID") 
	join "DBS" using ("DB_ID") 
	where "PARAM_KEY" = 'numRows' 
	ORDER BY "NAME", "PARAM_VALUE"::bigint DESC;


Analyse tables based on criteria
================================

It is also possible to get the table that are not analysed and produce hive
queries to mitigate that.

.. code:: sql

	select 'analyze table ' || "NAME" || '.' || "TBL_NAME" || ' compute statistics;'
            || ' analyze table ' || "NAME" || '.' || "TBL_NAME" || ' compute statistics for columns;' 
	from "TABLE_PARAMS" 
	join "TBLS" using ("TBL_ID") 
	join "DBS" using ("DB_ID") 
	where "PARAM_KEY" = 'numRows' 
	and "PARAM_VALUE" = '0'
	and "TBL_TYPE" = 'MANAGED_TABLE'
	ORDER BY "NAME", "PARAM_VALUE"::bigint DESC;
