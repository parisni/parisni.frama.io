<!--
.. title: vim based web browser
.. slug: vim-based-web-browser
.. date: 2020-01-18 16:37:50 UTC+01:00
.. tags: vim vivaldi qutebroser chromium firefox w3m
.. category:
.. link: 
.. description: 
.. type: text
-->

This post compares several open-source browser with vim
shorcuts. There is no perfect solution, however until now **vivaldi**
is the best solution I've found. In particular best:
  * hint links
  * newlines shortcut
  * vim integration
  * dark mode
  * add blocker


<!-- END_TEASER -->

# Qutebrowser

## Pros

- highly configurable
- many feature

## Cons

- complicated to install (qt, webkit)
- no good dark mode
- no good add blocker
- resource consuming

# Firefox (with tridactyl)

## Pros

- highly configurable
- good dark mode
- good add blocker

## Cons

- vim not totally integrated (configs)
- navigation bar not removable

# Chromium based

## How To

- use vivaldi (this is a fine tuneable version of chromium)
  - disable many shortkey
  - disable navigation bar
  - use self windows mode
- install *cvimium* : much better than vimium, and cvim. It handles newlines shortkeys within both vomnibar and input. It handles hints link better than any other.
- new tab, new windows: this opens tabs within windows
- dark mode:
  - dark reader: works in the general case
  - plus: "Just Black" theme (avoid blanck page accross pages)
- in i3, turn the resulting floating window to a sliding window
- also set as default browser: `xdg-mime default chromium-browser.desktop x-scheme-handler/https`  ([from](https://discourse.nixos.org/t/how-to-set-the-xdg-mime-default/3560/3?u=parisni))

## Pros

- good dark mode (combined)
- good vim mode + good url link
- good add blocker
- epured interface (no navigation bar)
- quite smooth navigation

## Cons

- vim not totally integrated (configs)
- complicated setup
- complicated to share config accross several computers
- the epured interface cannot be used for links

# w3m

## Pros
- native vim
- navive dark
- native add blocker
- show images

## Cons
- no video
- no javascript
