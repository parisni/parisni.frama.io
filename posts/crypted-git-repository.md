<!--
.. title: Crypted git repository
.. slug: crypted-git-repository
.. date: 2020-02-01 22:56:20 UTC+01:00
.. tags: git gpg
.. category: 
.. link: 
.. description: 
.. type: text
-->


Sensitive data shouldn't be lost. Git helps not loosing data.  Private
repository hide content from others. They don't hide from insiders
such system admins. Here are the steps to protect sensitive data with
git.

<!-- END_TEASER -->

*git-crypt* allows to crypt all or some of the files of a given git
repository with pgp. The name of the file will remain the same but the
content will be encrypted with one to many public keys. Only the
owner(s) of the private(s) key(s) will be able to read the content.

First, **git-crypt** needs to be installed together with **git** and
**gnupg**. Then create or reuse a *gpg key* and also a *git
repository* and run the below instruction:

```bash
git-crypt add-gpg-user <YOUR KEY ID>
```

The process is transparent by mean the files are en/decrypted on the
fly. The `.gitattributes` file need to be configured to specify which
files will be encrypted as following example:

``` 
.emacs.d/** filter=git-crypt diff=git-crypt
agenda.org filter=git-crypt diff=git-crypt
```

In case of conflict, the merging tool cannot make the difference. Also
the workaround described [in this issue](https://github.com/AGWA/git-crypt/issues/140#issuecomment-361031719)
does a great job.

Then don't forget to git add your `.gitattributes` and enjoy the
security on git !
