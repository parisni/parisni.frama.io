<!--
.. title: First contact with spark-jobserver
.. slug: first-contact-with-spark-jobserver
.. date: 2019-11-24 17:11:49 UTC+01:00
.. tags: spark, big-data
.. category: data engineering
.. link: 
.. description: 
.. type: text
-->

This describe how to test locally a spark-jobserver instance. [This derive from the readme](https://github.com/spark-jobserver/). First impressions are great.

<!-- END_TEASER-->

# Server side

```bash
git clone git@github.com:spark-jobserver/spark-jobserver.git
```

## Activate Basic Auth

```yaml
# add this at the end config/local.conf
shiro {
authentication = on
# absolute path to shiro config file, including file name
config.path = "config/shiro.ini.basic"
}
# specify the number of query per slot (default 8)
spark {
jobserver.max-jobs-per-context = 2
}
```

[More detail in the source code](https://github.com/spark-jobserver/spark-jobserver/blob/2eaa041070e5a4a2f97b01f8903c0f9207b0d452/job-server/src/test/scala/spark/jobserver/JavaJobSpec.scala)

```bash
sbt
# Run this
job-server-extras/reStart config/local.conf --- -Xmx8g
```

# Client

```bash
# compile the jar
sbt job-server-tests/package
# add the jar
curl --basic --user 'user:pwd' X POST localhost:8090/binaries/test -H "Content-Type: application/java-archive" --data-binary @job-server-tests/target/scala-2.11/job-server-tests_2.11-0.9.1-SNAPSHOT.jar
# pass the basic auth
curl localhost:8090/contexts --basic --user 'user:pwd' -d "" "localhost:8090/contexts/test-context?num-cpu-cores=4&memory-per-node=512m"
# get the contexts
curl -k --basic --user 'user:pw' https://localhost:8090/contexts
# get the jobs
curl -k --basic --user 'user:pw' https://localhost:8090/jobs
# run a job and increase the timeout
curl --basic --user 'user:pwd' -d "input.string = a b c a b see" "localhost:8090/jobs?appName=test&classPath=spark.jobserer.WordCountExample&context=test-context&sync=true&timeout=100000"
```


