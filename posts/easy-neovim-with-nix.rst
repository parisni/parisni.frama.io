.. title: Easy neovim with nix
.. slug: easy-neovim-with-nix
.. date: 2019-08-22 22:17:01 UTC+02:00
.. tags: vim
.. category: system administration
.. link: 
.. description: 
.. type: text

In this post, I will show how to maintain a neovim configuration with the
**nix** package manager. Nix can be installed on any linux distribution with a
single sudo bash script.

.. END_TEASER

One of the good point is it ease the way to install and configure softwares:
below is an example on how to install neomutt and several vim packages. In case
a package does not yet exists or is not up-to-date, the nixpkgs github
repository is the place to make pull requests.

In case the merge is accepted, the nix unstable channel will get the
modification few hours later, and any body is able to install the package.

Nix is well documented and the community looks extraordinary active. However
the syntax and what happens behind the scene is for now a source of curiosity
for me: I have not yet switch to **nixos**, and only be using the nix manager
on few servers.

Still some packages are bogus, but things looks moving fast and I such active
community makes the invest confident.

.. code-block:: haskell

    # .config/nixpkgs/config.nix
    {
      packageOverrides = pkgs: with pkgs; rec {
        myNeovim = neovim.override {
    	    vimAlias = true;
          configure = {
            customRC = ''
    	colo murphy
    	set number
    	set encoding=utf-8
    	au FileType python setlocal formatprg=autopep8\ -
    	
    	function! DoPrettyXML()
    	  let l:origft = &ft
    	  set ft=
    	  1s/<?xml .*?>//e
    	  0put ='<PrettyXML>'
    	  $put ='</PrettyXML>'
    	  silent %!xmllint --format -
    	  2d
    	  $d
    	  1
    	  exe "set ft=" . l:origft
    	endfunction
    	command! PrettyXML call DoPrettyXML()
            '';
            packages.myVimPackage = with pkgs.vimPlugins; {
              start = [ 
            airline
    	    vim-nix
            molokai
            tagbar
            easymotion
            nerdtree
            readline-vim
           ];
              opt = [ ];
            };      
        };
      };
        myPackages = pkgs.buildEnv {
          name = "myPackages";
          paths = [
            myNeovim
    	    vlc
          ];
        };
      };
    }

.. code-block:: bash 

   # it is possible to refresh the channels 
   nix-channel --update
   # it is possible to install an apply both vimrc/packages from this command:
   nix-env -i myPackages
   # it is possible to remove this by doing
   nix-env -e myPackages

