.. title: Playing with hive, spark & metastore versions
.. slug: playing-with-hive-spark-metastore-versions
.. date: 2019-05-30 16:33:11 UTC+02:00
.. tags: hive, big-data
.. category: data engineering
.. link: 
.. description: 
.. type: text

Hive has 3 versions. The last version offers faster queries, ACID and
constraints. However this new features have broken many projects bridges such
spark. So what's going on ?

.. END_TEASER

The **first one** deals with map reduce (mr) an tez as execution engine. It allows
acid transactions but the merge statement has `poor performances <https://mail-archives.apache.org/mod_mbox/hive-user/201805.mbox/browser>`_ compared to following versions. Spark is
fully compliant with hive 1, has for default metastore this version.

The **second one** deals with mr, tez, llap and mr3 as engine. It allows acid
transactions and under the hood create insert/update/delete chuncks files which
are computed at runtime. The acid tables are slow and get slower as number of
chuncks files grows. Major Compaction removes those files. Spark cannot only
read acid tables after a major compaction.

The **third one** deals with tez, llap and mr3 as engine. It allows fast acid
transactions and limit the number of files. Hive 3 offers primary keys, foreign
keys, and materialized views. Spark looks to deal with hive 3 metastore however
it is not officially supported. Both llap and mr3 provide reading and writing
bridge between spark and hive3 with dedicated library.

How to bridge spark and hive2
-----------------------------

hive provide a metastore tool to build the database. It is possible to get the
hive source for any version and build a metastore for the given version. Hive
needs hadoop installed to run that metastore tool.

Create the metastore
====================

.. code-block:: bash
	
	# clone hive and build
	mvn clean package -Pdist -DskipTests=true
	# download hadoop binary and unpack it then confifure the envir
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
	export HADOOP_HOME=/opt/hadoop/
	hive-rel-release-${hive.version}/packaging/target/apache-hive-${hive.version}-bin/apache-hive-${hive.version}-bin/bin/schematool -dbType derby -initSchema
	# this build a metastore_db of the current hive version


Then it is possible to run spark on that metastore (by simply starting the
spark-shell under the metastore_db folder). Then it is necessary to:

Configure the spark metastore
=============================

.. code-block:: bash
	
	#first step: get the metastore jars from maven
	spark-shell --conf spark.sql.hive.metastore.version=2.3 --conf spark.sql.hive.metastore.jars=maven --packages org.apache.zookeeper:zookeeper:3.4.6 
	# this download the needed jars in the java.io.temp folder
	# keep the jars somewhere
	cp -a /tmp/hive_2.3*/ /opt/spark/metastore_lib_v2.3/
	# yet it is possible to configure run spark directly without maven
	spark-shell --conf spark.sql.hive.metastore.version=2.3 --conf spark.sql.hive.metastore.jars=/opt/spark/metastore_v2.3/*


Configure a postgres metastore
==============================

The postgres database contains the details of the hive objects. The metastore
is a java service which use the default 9083 port. A postgresql backed
metastore allows spark to deal with hive3 (exept acid). Spark can connect to
the metastore with thrift without specifing the metastore version at all. Here
are the steps:

1. configure the hive-site.xml for postgres and postgres for hive. `This is described here <https://mapr.com/docs/60/Hive/Config-RemotePostgreSQLForHiveMetastore.html>`_
2. create the schema for the  metastore  and start the metastore

.. code-block:: bash

	bin/schematool  -dbType postgres  -initSchema
	bin/hive --service metastore

3. use the same hive-site.xml for spark


Conclusion
----------

Spark is able to connect to any metastore version right now (from 1 to 3). When
working with embebded derby metastore there is a need to configure spark. When
working with a postgres backed metastore, it is automatically handled.





