.. title: Publish a Scala project on Maven Central
.. slug: publish-a-scala-project-on-maven-central
.. date: 2019-06-09 19:56:10 UTC+02:00
.. tags: maven, scala
.. category: software engineering
.. link: 
.. description: 
.. type: text

Having a public repository to store your programs is convenient. Maven Central
is a public repository for java code base. The whole process is not straight
forward.

.. END_TEASER

Register to sonatype
=====================

I am not sure what exactly is sonatype as an organisation, but at least they
provide a nexus and a jira to let user push their artifacts on maven. They also
provide assistance for your fist release. This process has been well described
on `this post <http://mbcoder.com/publishing-to-maven-central/>`_ 

Configure the project
======================

Besides the new tools to deal with and the surprising process with sonatype,
the main complexity is the project setup with the pom.xml.

Add sonatype references
-------------------------

.. CODE:: xml

	<!-- pom.xml -->
	<developers>
	  <developer>
	    <name>user</name>
	    <email>email</email>
	  </developer>
	</developers>
	<distributionManagement>
	  <snapshotRepository>
	    <id>ossrh</id>
	    <url>https://oss.sonatype.org/content/repositories/snapshots</url>
	  </snapshotRepository>
	  <repository>
	    <id>ossrh</id>
	    <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
	  </repository>
	</distributionManagement>

.. CODE:: xml

	<!-- setting.xml -->
	<servers>
	  <server>
	    <id>ossrh</id>
	    <username>username</username>
	    <password>token</password>
	  </server>
	</servers>


Sign the sources with pgp
-------------------------

The maven settings.xml file, the pom.xml and a public/private gpg key have to be setup.

.. CODE:: xml

	<plugin>
	  <groupId>org.apache.maven.plugins</groupId>
	  <artifactId>maven-gpg-plugin</artifactId>
	  <version>1.6</version>
	  <executions>
	    <execution>
	      <id>sign-artifacts</id>
	      <phase>verify</phase>
	      <goals>
	        <goal>sign</goal>
	      </goals>
	      <configuration>
	        <keyname>${gpg.keyname}</keyname>
	        <passphraseServerId>${gpg.keyname}</passphraseServerId>
	      </configuration>
	    </execution>
	  </executions>
	</plugin>


Add the javadoc
---------------

Nexus rules ask for javadoc being uploaded. Scala does not produce javadoc but
scaladoc. It is possible to create javadoc from scaladoc thanks to the
`genjavadoc <https://github.com/lightbend/genjavadoc>`_ library.

.. CODE:: bash

	# the maven goal need to specify the profile **javadoc**
	javadoc:aggregate -Pjavadoc \ # generate the javadoc the scaladoc

.. CODE:: xml

	<profile>
	  <id>javadoc</id>
	  <build>
	    <plugins>
	      <plugin>
	        <groupId>net.alchim31.maven</groupId>
	        <artifactId>scala-maven-plugin</artifactId>
	        <executions>
	          <execution>
	            <id>doc</id>
	            <phase>generate-sources</phase>
	            <goals>
	              <goal>compile</goal>
	            </goals>
	          </execution>
	        </executions>
	        <configuration>
	          <args>
	            <arg>-P:genjavadoc:out=${project.build.directory}/genjavadoc</arg>
	          </args>
	          <compilerPlugins>
	            <compilerPlugin>
	              <groupId>com.typesafe.genjavadoc</groupId>
	              <artifactId>genjavadoc-plugin_${scala.binary.full.version}</artifactId>
	              <version>0.11</version>
	            </compilerPlugin>
	          </compilerPlugins>
	        </configuration>
	      </plugin>
	      <plugin>
	        <groupId>org.codehaus.mojo</groupId>
	        <artifactId>build-helper-maven-plugin</artifactId>
	        <executions>
	          <execution>
	            <phase>generate-sources</phase>
	            <goals>
	              <goal>add-source</goal>
	            </goals>
	            <configuration>
	              <sources>
	                <source>${project.build.directory}/genjavadoc</source>
	              </sources>
	            </configuration>
	          </execution>
	        </executions>
	      </plugin>
	      <plugin>
	        <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-javadoc-plugin</artifactId>
	        <version>2.9</version>
	        <configuration>
	          <minmemory>64m</minmemory>
	          <maxmemory>2g</maxmemory>
	          <outputDirectory>${project.build.directory}</outputDirectory>
	          <detectLinks>true</detectLinks>
	        </configuration>
	      </plugin>
	    </plugins>
	  </build>
	</profile>
	</profiles>

Maven command to deploy
=======================

.. CODE:: bash

	# generate the scaladoc
	scaladoc src/main/*.scala

.. CODE:: bash

	# deploy
	mvn clean install 
	mvn  javadoc:aggregate -Pjavadoc javadoc:jar source:jar javadoc:aggregate -Pjavadoc deploy

Conclusion
===========

This procedure is a pain in the ass and I hope I will be more familiar with those stuff soon.

