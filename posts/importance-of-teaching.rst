.. title: Importance of teaching
.. slug: importance-of-teaching
.. date: 2019-07-07 17:08:12 UTC+02:00
.. tags: 
.. category: lifestyle
.. link: 
.. description: 
.. type: text

Today knowledge **exists**. Search-engine and recomendation-algorithms makes
knowledge **accessible**. We live in a knowledge **opulence**. Does this mean
we make a good use of the knowledge ?

.. END_TEASER

Opulence is counter productive
==============================

.. image:: /images/fish.jpg

For multiple reason among reducing consumption from predatory, fish use
**schooling**: they regroup and swim in the same direction. An interpretation
is that the predator is less able to make choice in this context of opulence.

Paradoxally, living in a megalopole usually increase lonely and people decrease
their activity. Opulence of offer limit the usage.

I am pretty sure the opulence of information on the web acts in the same
manner. The potential is huge, but the usage is low.

Faster, but less emotion
=========================

Before internet, knowledge was physically shared among people or books. The
knowledge came together with an emotional context. Some memory method use
emotion to ehnance performances.

Internet knowledge does not provide such emotion together with the information.
Therefore, the memory processus cannot be enhanced naturally. The is a need to
reinforce the internet knowledges.

Teaching
========

There is multiple ways of teaching: explaining to others, writing a blog-post,
self explaining by mind. In every cases, teaching need a synthesis step, and
structure the informations to improve understanding and finally ease the
learning.

Teaching is a powerful way of consolidating the knowledges. It helps to raise
error or superficial understanding. It has interesting advantages: the teaching
materials can be used to re-activate knowledges, and also reduce the cognitive
load.

Conclusion
==========

In the days of knowledge opulence, there is a numeric hygiene to fix the
informations and take advantage of it.
