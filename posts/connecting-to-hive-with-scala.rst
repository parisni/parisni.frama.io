.. title: Connecting to hive with scala
.. slug: connecting-to-hive-with-scala
.. date: 2019-05-31 15:12:42 UTC+02:00
.. tags: hive, big-data
.. category: data engineering
.. link: 
.. description: 
.. type: text


Hive has made large improvements this days. Tools such spark or pig cannot deal
with transactionnal tables. It is necessary to run some hive queries with the
hive engine as a workaround to manipulate transactions. Here are two ways to
run SQL queries from scala. Finally, I only have been able to use the second
one.

.. END_TEASER

The Metastore way
-----------------

Recipe
======

Apache spark connects to hive metastore and hdfs directly. This is possible to
do the same with raw scala code. As well as spark, the hive-site.xml file shall
be within the classpath. There is also the need for the engine configuration
(tez, llap) to be effective otherwise the default MR engine will be used. I
have not been able to find out how to configure TEZ.  


The JDBC way
-------------

Recipe
=======

- a hiveServer2 instance running
- a security method (here kerberos)
- a hive-jdbc driver

The JDBC connection is advised on the hive wiki. It is more secured because the
client does not need a direct access to hdfs and the metastore. The JDBC driver
needs to be loaded and the JDBC url configured propertly. Also the security
method (here kerberos) needs to be done. The below code illustrates the way to go:

.. code:: scala

	
	package fr.aphp.wind.eds.spark.etl
	
	import java.sql.DriverManager
	import java.sql.Connection
	
	class HIVEUtil(principal: String, keytab: String, url: String) {
	  var connection: Connection = null
	
	  def init() = {
	    val conf = new org.apache.hadoop.conf.Configuration();
	    conf.set("hadoop.security.authentication", "Kerberos");
	    org.apache.hadoop.security.UserGroupInformation.setConfiguration(conf);
	    org.apache.hadoop.security.UserGroupInformation.loginUserFromKeytab(principal, keytab);
	    val driver = "org.apache.hive.jdbc.HiveDriver"
	    Class.forName(driver)
	    connection = DriverManager.getConnection(url)
	  }
	
	  def execute(query: String) = {
	    val statement = connection.createStatement()
	    statement.execute(query)
	  }
	
	  def executeQuery(query: String): ResultSet = {
	    val statement = connection.createStatement()
	    val resultSet = statement.executeQuery(query)
	    resultSet
	  }
	
	  def close() = {
	    connection.close()
	  }
	
	}


Conclusion
-----------

Using the JDBC driver to run HQL queries is fairly easier to setup than the
metastore server.
