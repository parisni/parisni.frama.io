.. title: Rich Structured Text and blogging
.. slug: rich-structured-text-and-blogging
.. date: 2019-06-01 23:39:43 UTC+02:00
.. tags: nikola
.. category: website development
.. link: 
.. description: 
.. type: text


In this post, I will illustrate useful RST syntax to produce blog content. They
are not easy to remember, but powerful.


Hyperlinks
===========

`Look at this video <https://video.ploud.fr/videos/embed/8f3d6f0b-6452-4a72-8187-19d1f60879b4>`_

Here is the RST code :

.. code:: rst

	`Look at this video <https://video.ploud.fr/videos/embed/8f3d6f0b-6452-4a72-8187-19d1f60879b4>`_


Embedded videos
===============

.. raw:: html

	<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.ploud.fr/videos/embed/8f3d6f0b-6452-4a72-8187-19d1f60879b4" frameborder="0" allowfullscreen></iframe>


Here is the RST code to get the result. The iframe code was copied the peertube website:

.. code:: rst

	.. raw:: html
	
		<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.ploud.fr/videos/embed/8f3d6f0b-6452-4a72-8187-19d1f60879b4" frameborder="0" allowfullscreen></iframe>
